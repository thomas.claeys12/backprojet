<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\File;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Class FileController
 * @package App\Controller
 * @Route("/api/file", name="file")
 */
class FileController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @Route(name="file_upload",path="/upload",methods={"post"})
     */
    public function fileUpload(request $request, SerializerInterface $serializer)
    {
        $data = json_decode($request->getContent());
        $uploadedFile = $data->image;
        $nameFile = $data->nameFile;
        $user = $data->user;
        $repository = $this->doctrine
            ->getManager()
            ->getRepository(User::class);
        $userv2 = $repository->find($user->id);
        $file = new File();
        $file->setName($nameFile);
        $file->setPath($uploadedFile);
        $file->setUser($userv2);
        $this->doctrine->getManager()->persist($file);
        $this->doctrine->getManager()->flush();

        return new JsonResponse($serializer->serialize("done", 'json'), 200, [], true);
    }
}
